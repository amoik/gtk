/* GTK - The GIMP Toolkit
 *
 * gtkvulkanarea.h: A Vulkan drawing area
 *
 * Copyright © 2022  Andreas Moik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GTK_VULKAN_AREA_H__
#define __GTK_VULKAN_AREA_H__

#if !defined(__GTK_H_INSIDE__) && !defined(GTK_COMPILATION)
#error "Only <gtk/gtk.h> can be included directly."
#endif

#include <gtk/gtkwidget.h>

#include <vulkan/vulkan.h>

G_BEGIN_DECLS

#define GTK_TYPE_VULKAN_AREA (gtk_vulkan_area_get_type ())
#define GTK_VULKAN_AREA(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_VULKAN_AREA, GtkVulkanArea))
#define GTK_IS_VULKAN_AREA(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_VULKAN_AREA))
#define GTK_VULKAN_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_VULKAN_AREA, GtkVulkanAreaClass))
#define GTK_IS_VULKAN_AREA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_VULKAN_AREA))
#define GTK_VULKAN_AREA_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_VULKAN_AREA, GtkVulkanAreaClass))

typedef struct _GtkVulkanArea GtkVulkanArea;
typedef struct _GtkVulkanAreaClass GtkVulkanAreaClass;

struct _GtkVulkanArea
{
  /*< private >*/
  GtkWidget parent_instance;
};

struct _GtkVulkanAreaClass
{
  /*< private >*/
  GtkWidgetClass parent_class;

  /*< public >*/
  void (*render) (GtkVulkanArea *area, VkCommandBuffer commandBuffer, size_t frame);
  void (*resize) (GtkVulkanArea *area,
                  int width,
                  int height);
};

GDK_AVAILABLE_IN_ALL
GType gtk_vulkan_area_get_type (void) G_GNUC_CONST;

GDK_AVAILABLE_IN_ALL
GtkWidget *gtk_vulkan_area_new (void);

GDK_AVAILABLE_IN_ALL
bool
gtk_vulkan_area_vk_check_fatal (int result);

GDK_AVAILABLE_IN_ALL
VkInstance gtk_vulkan_area_get_instance (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
VkPhysicalDevice gtk_vulkan_area_get_physical_device (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
VkDevice gtk_vulkan_area_get_device (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
VkQueue gtk_vulkan_area_get_queue (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
VkCommandPool gtk_vulkan_area_get_command_pool (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
VkRenderPass gtk_vulkan_area_get_render_pass (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
uint32_t gtk_vulkan_area_get_swap_chain_size (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
int gtk_vulkan_area_get_width (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
int gtk_vulkan_area_get_height (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_set_application_name (GtkVulkanArea *area,
                                           const char *application_name);
GDK_AVAILABLE_IN_ALL
const char *gtk_vulkan_area_get_application_name (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_set_engine_name (GtkVulkanArea *area,
                                      const char *engine_name);
GDK_AVAILABLE_IN_ALL
const char *gtk_vulkan_area_get_engine_name (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_set_validation_layers (GtkVulkanArea *area,
                                            const char *validation_layers);
GDK_AVAILABLE_IN_ALL
const char *gtk_vulkan_area_get_validation_layers (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_set_extensions (GtkVulkanArea *area,
                                     const char *extensions);
GDK_AVAILABLE_IN_ALL
const char *gtk_vulkan_area_get_extensions (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_set_render_permanently (GtkVulkanArea *area,
                                             gboolean render_permanently);
GDK_AVAILABLE_IN_ALL
gboolean gtk_vulkan_area_get_render_permanently (GtkVulkanArea *area);

GDK_AVAILABLE_IN_ALL
void gtk_vulkan_area_queue_render (GtkVulkanArea *area);

G_END_DECLS

#endif /* __GTK_VULKAN_AREA_H__ */
