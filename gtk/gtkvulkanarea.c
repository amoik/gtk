/* GTK - The GIMP Toolkit
 *
 * gtkvulkanarea.c: A Vulkan drawing area
 *
 * Copyright © 2022  Andreas Moik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "config.h"
#include "gtkmarshalers.h"
#include "gtknative.h"
#include "gtkprivate.h"
#include "gtksnapshot.h"
#include "gtkvulkanarea.h"
#include "gtkwidgetprivate.h"
/**
 * GtkVulkanArea:
 *
 * `GtkVulkanArea` is a widget that allows drawing with Vulkan.
 *
 */

#define Q(x) #x
#define QUOTE(x) Q (x)

#define VK_RESULT_CASE(name)     \
  case name:                     \
    {                            \
      g_critical (QUOTE (name)); \
      return false;              \
    }

typedef struct
{
  VkImage image;
  VkDeviceMemory memory;
  VkImageView view;
} FrameBufferAttachment;

typedef struct
{
  gchar *application_name;
  gchar *engine_name;

  uint32_t width;
  uint32_t height;

  char *validation_layers;
  char *extensions;

  VkFormat color_format;
  VkFormat depth_format;

  uint32_t swap_chain_size;
  VkCommandBuffer current_command_buffer;
  uint32_t current_frame;
  VkInstance instance;
  VkPhysicalDevice physical_device;
  VkDevice device;
  VkQueue queue;
  VkCommandPool command_pool;
  VkRenderPass render_pass;
  VkImage dst_image;
  VkDeviceMemory dst_image_memory;
  uint8_t *pixels;
  FrameBufferAttachment color_attachment, depth_attachment;
  VkFramebuffer framebuffer;

  gboolean needs_resize;
  gboolean needs_render;
  guint tick_id;
  gboolean initialized;

  void (*on_error) (gchar *);

} GtkVulkanAreaPrivate;

enum
{
  PROP_0,

  PROP_APPLICATION_NAME,
  PROP_ENGINE_NAME,
  PROP_RENDER_PERMANENTLY,
  PROP_EXTENSIONS,
  PROP_VALIDATION_LAYERS,

  LAST_PROP
};

static GParamSpec *obj_props[LAST_PROP] = {
  NULL,
};

enum
{
  RENDER,
  RESIZE,

  LAST_SIGNAL
};

void
gtk_vulkan_area_insert_image_memory_barrier (VkCommandBuffer cmdbuffer,
                                             VkImage image,
                                             VkAccessFlags src_access_mask,
                                             VkAccessFlags dst_access_mask,
                                             VkImageLayout old_image_layout,
                                             VkImageLayout new_image_layout,
                                             VkPipelineStageFlags src_stage_mask,
                                             VkPipelineStageFlags dst_stage_mask,
                                             VkImageSubresourceRange subresource_range);
VkMemoryAllocateInfo
gtk_vulkan_area_memory_allocate_info (void);
uint32_t
gtk_vulkan_area_get_memory_type_index (uint32_t type_bits, VkMemoryPropertyFlags properties, VkPhysicalDevice physical_device);
VkBool32
gtk_vulkan_area_get_supported_depth_format (VkPhysicalDevice physical_device, VkFormat *depth_format);
char **
gtk_vulkan_area_split_string_new (const char *str, uint32_t *count);
bool
gtk_vulkan_area_set_depth_format (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_instance (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_physical_device (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_device_queue_and_command_pool (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_render_pass (GtkVulkanArea *area);
void
gtk_vulkan_area_destroy_render_pass (GtkVulkanArea *area);
void
gtk_vulkan_area_destroy_framebuffer_attachments (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_framebuffer_attachments (GtkVulkanArea *area);
bool
gtk_vulkan_area_create_framebuffer (GtkVulkanArea *area);
void
gtk_vulkan_area_destroy_framebuffer (GtkVulkanArea *area);

static guint area_signals[LAST_SIGNAL] = {
  0,
};

G_DEFINE_TYPE_WITH_PRIVATE (GtkVulkanArea, gtk_vulkan_area, GTK_TYPE_WIDGET)

bool
gtk_vulkan_area_vk_check_fatal (int result)
{
  switch (result)
    {
    case VK_SUCCESS:
      {
        return true;
      }

      VK_RESULT_CASE (VK_NOT_READY)
      VK_RESULT_CASE (VK_TIMEOUT)
      VK_RESULT_CASE (VK_EVENT_SET)
      VK_RESULT_CASE (VK_EVENT_RESET)
      VK_RESULT_CASE (VK_INCOMPLETE)
      VK_RESULT_CASE (VK_ERROR_OUT_OF_HOST_MEMORY)
      VK_RESULT_CASE (VK_ERROR_OUT_OF_DEVICE_MEMORY)
      VK_RESULT_CASE (VK_ERROR_INITIALIZATION_FAILED)
      VK_RESULT_CASE (VK_ERROR_DEVICE_LOST)
      VK_RESULT_CASE (VK_ERROR_MEMORY_MAP_FAILED)
      VK_RESULT_CASE (VK_ERROR_LAYER_NOT_PRESENT)
      VK_RESULT_CASE (VK_ERROR_EXTENSION_NOT_PRESENT)
      VK_RESULT_CASE (VK_ERROR_FEATURE_NOT_PRESENT)
      VK_RESULT_CASE (VK_ERROR_INCOMPATIBLE_DRIVER)
      VK_RESULT_CASE (VK_ERROR_TOO_MANY_OBJECTS)
      VK_RESULT_CASE (VK_ERROR_FORMAT_NOT_SUPPORTED)
      VK_RESULT_CASE (VK_ERROR_FRAGMENTED_POOL)
      VK_RESULT_CASE (VK_ERROR_OUT_OF_POOL_MEMORY)
      VK_RESULT_CASE (VK_ERROR_INVALID_EXTERNAL_HANDLE)
      VK_RESULT_CASE (VK_ERROR_SURFACE_LOST_KHR)
      VK_RESULT_CASE (VK_ERROR_NATIVE_WINDOW_IN_USE_KHR)
      VK_RESULT_CASE (VK_SUBOPTIMAL_KHR)
      VK_RESULT_CASE (VK_ERROR_OUT_OF_DATE_KHR)
      VK_RESULT_CASE (VK_ERROR_INCOMPATIBLE_DISPLAY_KHR)
      VK_RESULT_CASE (VK_ERROR_VALIDATION_FAILED_EXT)
      VK_RESULT_CASE (VK_ERROR_INVALID_SHADER_NV)
      VK_RESULT_CASE (VK_ERROR_NOT_PERMITTED_EXT)
      VK_RESULT_CASE (VK_ERROR_FRAGMENTATION)
      VK_RESULT_CASE (VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS)
      VK_RESULT_CASE (VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT)
      VK_RESULT_CASE (VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT)

#if VK_HEADER_VERSION >= 220
      VK_RESULT_CASE (VK_ERROR_PIPELINE_COMPILE_REQUIRED_EXT)
      VK_RESULT_CASE (VK_THREAD_IDLE_KHR)
      VK_RESULT_CASE (VK_THREAD_DONE_KHR)
      VK_RESULT_CASE (VK_OPERATION_DEFERRED_KHR)
      VK_RESULT_CASE (VK_OPERATION_NOT_DEFERRED_KHR)
      VK_RESULT_CASE (VK_ERROR_COMPRESSION_EXHAUSTED_EXT)
#endif

    case VK_ERROR_UNKNOWN:
    case VK_RESULT_MAX_ENUM:
    default:
      {
        char error[50];
        sprintf (error, "An unknown Vulkan error occured: #%d", result);
        g_critical ("%s", error);
      }
    }
  return false;
}

void
gtk_vulkan_area_insert_image_memory_barrier (VkCommandBuffer cmdbuffer,
                                             VkImage image,
                                             VkAccessFlags src_access_mask,
                                             VkAccessFlags dst_access_mask,
                                             VkImageLayout old_image_layout,
                                             VkImageLayout new_image_layout,
                                             VkPipelineStageFlags src_stage_mask,
                                             VkPipelineStageFlags dst_stage_mask,
                                             VkImageSubresourceRange subresource_range)
{
  VkImageMemoryBarrier image_memory_barrier;
  image_memory_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  image_memory_barrier.pNext = NULL;
  image_memory_barrier.srcAccessMask = src_access_mask;
  image_memory_barrier.dstAccessMask = dst_access_mask;
  image_memory_barrier.oldLayout = old_image_layout;
  image_memory_barrier.newLayout = new_image_layout;
  image_memory_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  image_memory_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  image_memory_barrier.image = image;
  image_memory_barrier.subresourceRange = subresource_range;

  vkCmdPipelineBarrier (cmdbuffer, src_stage_mask, dst_stage_mask, 0, 0, NULL, 0, NULL, 1, &image_memory_barrier);
}

VkMemoryAllocateInfo
gtk_vulkan_area_memory_allocate_info (void)
{
  VkMemoryAllocateInfo mem_alloc_info;
  mem_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  mem_alloc_info.pNext = NULL;
  mem_alloc_info.allocationSize = 0;
  mem_alloc_info.memoryTypeIndex = 0;
  return mem_alloc_info;
}

uint32_t
gtk_vulkan_area_get_memory_type_index (uint32_t type_bits, VkMemoryPropertyFlags properties, VkPhysicalDevice physical_device)
{
  VkPhysicalDeviceMemoryProperties device_memory_properties;
  vkGetPhysicalDeviceMemoryProperties (physical_device, &device_memory_properties);
  for (uint32_t i = 0; i < device_memory_properties.memoryTypeCount; i++)
    {
      if ((type_bits & 1) == 1)
        {
          if ((device_memory_properties.memoryTypes[i].propertyFlags & properties) == properties)
            {
              return i;
            }
        }
      type_bits >>= 1;
    }
  return 0;
}

VkBool32
gtk_vulkan_area_get_supported_depth_format (VkPhysicalDevice physical_device, VkFormat *depth_format)
{
  // find a depth format
  VkFormat formats[] = { VK_FORMAT_D32_SFLOAT_S8_UINT,
                         VK_FORMAT_D32_SFLOAT,
                         VK_FORMAT_D24_UNORM_S8_UINT,
                         VK_FORMAT_D16_UNORM_S8_UINT,
                         VK_FORMAT_D16_UNORM };

  for (unsigned int i = 0; i < sizeof (formats) / sizeof (formats[0]); i++)
    {
      VkFormatProperties format_props;
      vkGetPhysicalDeviceFormatProperties (physical_device, formats[i], &format_props);

      if (format_props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
        {
          *depth_format = formats[i];
          return true;
        }
    }

  return false;
}

char **
gtk_vulkan_area_split_string_new (const char *str, uint32_t *count)
{
  char **ret = NULL;
  *count = 0;

  if (str)
    {
      char *dup = strdup (str);

      const char *delim = ",";
      const char *split_counter = strtok (dup, delim);

      while (split_counter != NULL)
        {
          *count += 1;
          split_counter = strtok (NULL, delim);
        }

      free (dup);

      if (*count > 0)
        {
          ret = malloc (sizeof (char *) * *count);
          dup = strdup (str);
          char *split = strtok (dup, delim);
          int i = 0;
          while (split != NULL)
            {
              ret[i++] = strdup (split);
              split = strtok (NULL, delim);
            }
          free (dup);
        }
    }
  return ret;
}

bool
gtk_vulkan_area_set_depth_format (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);
  if (!gtk_vulkan_area_get_supported_depth_format (priv->physical_device, &priv->depth_format))
    {
      if (priv->on_error)
        {
          char error[30];
          sprintf (error, "could not get depth format");
          (*priv->on_error) (error);
        }
      return false;
    }
  return true;
}

bool
gtk_vulkan_area_create_instance (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  VkApplicationInfo app_info = {};
  app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
  app_info.pApplicationName = priv->application_name;
  app_info.pEngineName = priv->engine_name;
  app_info.apiVersion = VK_API_VERSION_1_0;

  VkInstanceCreateInfo instance_create_info;

  instance_create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
  instance_create_info.pNext = NULL;
  instance_create_info.flags = 0;
  instance_create_info.pApplicationInfo = &app_info;
  char **layers = gtk_vulkan_area_split_string_new (priv->validation_layers, &instance_create_info.enabledLayerCount);
  instance_create_info.ppEnabledLayerNames = (const char **) layers;
  char **extensions = gtk_vulkan_area_split_string_new (priv->extensions, &instance_create_info.enabledExtensionCount);
  instance_create_info.ppEnabledExtensionNames = (const char **) extensions;

  bool ret = gtk_vulkan_area_vk_check_fatal (vkCreateInstance (&instance_create_info, NULL, &priv->instance));

  for (uint32_t i = 0; i < instance_create_info.enabledLayerCount; i++)
    free (layers[i]);
  for (uint32_t i = 0; i < instance_create_info.enabledExtensionCount; i++)
    free (extensions[i]);

  free (layers);
  free (extensions);

  return ret;
}

bool
gtk_vulkan_area_create_physical_device (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  // Vulkan device creation
  uint32_t device_count = 0;
  if (!gtk_vulkan_area_vk_check_fatal (vkEnumeratePhysicalDevices (priv->instance, &device_count, NULL)))
    return false;

  VkPhysicalDevice *physical_devices = malloc (sizeof (VkPhysicalDevice) * device_count);
  if (!gtk_vulkan_area_vk_check_fatal (vkEnumeratePhysicalDevices (priv->instance, &device_count, physical_devices)))
    {
      free (physical_devices);
      return false;
    }

  priv->physical_device = physical_devices[0];
  free (physical_devices);

  return true;
}

bool
gtk_vulkan_area_create_device_queue_and_command_pool (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties (priv->physical_device, &device_properties);

  const float default_queue_priority = 0.0f;
  VkDeviceQueueCreateInfo queue_create_info;
  bool queue_found = false;
  uint32_t queue_family_index = 0;
  uint32_t queue_family_count;
  vkGetPhysicalDeviceQueueFamilyProperties (priv->physical_device, &queue_family_count, NULL);
  VkQueueFamilyProperties *queue_family_properties = malloc (sizeof (VkQueueFamilyProperties) * queue_family_count);
  vkGetPhysicalDeviceQueueFamilyProperties (priv->physical_device, &queue_family_count, queue_family_properties);
  for (uint32_t i = 0; i < queue_family_count; i++)
    {
      if (queue_family_properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
          queue_family_index = i;
          queue_create_info.pQueuePriorities = &default_queue_priority;
          queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
          queue_create_info.pNext = NULL;
          queue_create_info.flags = 0;
          queue_create_info.queueFamilyIndex = i;
          queue_create_info.queueCount = 1;

          queue_found = true;
          break;
        }
    }
  free (queue_family_properties);

  if (!queue_found)
    return false;

  // Create logical device
  VkDeviceCreateInfo device_create_info;

  device_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  device_create_info.pNext = NULL;
  device_create_info.flags = 0;
  device_create_info.queueCreateInfoCount = 1;
  device_create_info.pQueueCreateInfos = &queue_create_info;
  device_create_info.enabledLayerCount = 0;
  device_create_info.ppEnabledLayerNames = NULL;
  device_create_info.enabledExtensionCount = 0;
  device_create_info.ppEnabledExtensionNames = NULL;
  device_create_info.pEnabledFeatures = NULL;

  if (!gtk_vulkan_area_vk_check_fatal (vkCreateDevice (priv->physical_device, &device_create_info, NULL, &priv->device)))
    return false;

  // Get a graphics queue
  vkGetDeviceQueue (priv->device, queue_family_index, 0, &priv->queue);

  // Command pool
  VkCommandPoolCreateInfo cmd_poolInfo = {};
  cmd_poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cmd_poolInfo.pNext = NULL;
  cmd_poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  cmd_poolInfo.queueFamilyIndex = queue_family_index;
  if (!gtk_vulkan_area_vk_check_fatal (vkCreateCommandPool (priv->device, &cmd_poolInfo, NULL, &priv->command_pool)))
    return false;

  return true;
}

bool
gtk_vulkan_area_create_render_pass (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  VkAttachmentDescription attchment_descriptions[2];
  // Color attachment
  attchment_descriptions[0].flags = 0;
  attchment_descriptions[0].format = priv->color_format;
  attchment_descriptions[0].samples = VK_SAMPLE_COUNT_1_BIT;
  attchment_descriptions[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attchment_descriptions[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attchment_descriptions[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attchment_descriptions[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attchment_descriptions[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attchment_descriptions[0].finalLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
  // Depth attachment
  attchment_descriptions[1].flags = 0;
  attchment_descriptions[1].format = priv->depth_format;
  attchment_descriptions[1].samples = VK_SAMPLE_COUNT_1_BIT;
  attchment_descriptions[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attchment_descriptions[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attchment_descriptions[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attchment_descriptions[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attchment_descriptions[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attchment_descriptions[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkAttachmentReference color_reference = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
  VkAttachmentReference depth_reference = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

  VkSubpassDescription subpass_description;
  subpass_description.flags = 0;
  subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpass_description.inputAttachmentCount = 0;
  subpass_description.pInputAttachments = NULL;
  subpass_description.colorAttachmentCount = 1;
  subpass_description.pColorAttachments = &color_reference;
  subpass_description.pResolveAttachments = NULL;
  subpass_description.pDepthStencilAttachment = &depth_reference;
  subpass_description.preserveAttachmentCount = 0;
  subpass_description.pPreserveAttachments = NULL;

  // Use subpass dependencies for layout transitions
  VkSubpassDependency dependencies[2];
  dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[0].dstSubpass = 0;
  dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
  dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
  dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  dependencies[1].srcSubpass = 0;
  dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
  dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
  dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  // Create the actual renderpass
  VkRenderPassCreateInfo render_pass_info;
  render_pass_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  render_pass_info.pNext = NULL;
  render_pass_info.flags = 0;
  render_pass_info.attachmentCount = sizeof (attchment_descriptions) / sizeof (attchment_descriptions[0]);
  render_pass_info.pAttachments = attchment_descriptions;
  render_pass_info.subpassCount = 1;
  render_pass_info.pSubpasses = &subpass_description;
  render_pass_info.dependencyCount = sizeof (dependencies) / sizeof (dependencies[0]);
  render_pass_info.pDependencies = dependencies;

  if (!gtk_vulkan_area_vk_check_fatal (vkCreateRenderPass (priv->device, &render_pass_info, NULL, &priv->render_pass)))
    return false;

  return true;
}

void
gtk_vulkan_area_destroy_render_pass (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  if (priv->render_pass)
    vkDestroyRenderPass (priv->device, priv->render_pass, NULL);
}

void
gtk_vulkan_area_destroy_framebuffer_attachments (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  if (priv->color_attachment.image)
    vkDestroyImage (priv->device, priv->color_attachment.image, NULL);
  if (priv->depth_attachment.image)
    vkDestroyImage (priv->device, priv->depth_attachment.image, NULL);

  if (priv->color_attachment.view)
    vkDestroyImageView (priv->device, priv->color_attachment.view, NULL);
  if (priv->depth_attachment.view)
    vkDestroyImageView (priv->device, priv->depth_attachment.view, NULL);

  if (priv->color_attachment.memory)
    {
      vkFreeMemory (priv->device, priv->color_attachment.memory, NULL);
      priv->color_attachment.memory = NULL;
    }

  if (priv->depth_attachment.memory)
    {
      vkFreeMemory (priv->device, priv->depth_attachment.memory, NULL);
      priv->depth_attachment.memory = NULL;
    }
}

bool
gtk_vulkan_area_create_framebuffer_attachments (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  // Color attachment
  VkImageCreateInfo image;
  image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  image.pNext = NULL;
  image.flags = 0;
  image.imageType = VK_IMAGE_TYPE_2D;
  image.format = priv->color_format;
  image.extent.width = priv->width;
  image.extent.height = priv->height;
  image.extent.depth = 1;
  image.mipLevels = 1;
  image.arrayLayers = 1;
  image.samples = VK_SAMPLE_COUNT_1_BIT;
  image.tiling = VK_IMAGE_TILING_OPTIMAL;
  image.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
  image.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
  image.queueFamilyIndexCount = 0;
  image.pQueueFamilyIndices = NULL;
  image.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

  {
    VkMemoryRequirements mem_reqs;
    if (!gtk_vulkan_area_vk_check_fatal (vkCreateImage (priv->device, &image, NULL, &priv->color_attachment.image)))
      return false;
    vkGetImageMemoryRequirements (priv->device, priv->color_attachment.image, &mem_reqs);

    VkMemoryAllocateInfo mem_alloc_info = gtk_vulkan_area_memory_allocate_info ();
    mem_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc_info.pNext = NULL;
    mem_alloc_info.allocationSize = 0;
    mem_alloc_info.memoryTypeIndex = gtk_vulkan_area_get_memory_type_index (mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, priv->physical_device);
    mem_alloc_info.allocationSize = mem_reqs.size;

    if (!gtk_vulkan_area_vk_check_fatal (vkAllocateMemory (priv->device, &mem_alloc_info, NULL, &priv->color_attachment.memory)))
      return false;
    if (!gtk_vulkan_area_vk_check_fatal (vkBindImageMemory (priv->device, priv->color_attachment.image, priv->color_attachment.memory, 0)))
      return false;
  }

  // Depth stencil attachment
  {
    image.format = priv->depth_format;
    image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

    VkMemoryRequirements mem_reqs;
    if (!gtk_vulkan_area_vk_check_fatal (vkCreateImage (priv->device, &image, NULL, &priv->depth_attachment.image)))
      return false;
    vkGetImageMemoryRequirements (priv->device, priv->depth_attachment.image, &mem_reqs);

    VkMemoryAllocateInfo mem_alloc_info = gtk_vulkan_area_memory_allocate_info ();
    mem_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc_info.pNext = NULL;
    mem_alloc_info.allocationSize = 0;
    mem_alloc_info.memoryTypeIndex = gtk_vulkan_area_get_memory_type_index (mem_reqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, priv->physical_device);
    mem_alloc_info.allocationSize = mem_reqs.size;

    if (!gtk_vulkan_area_vk_check_fatal (vkAllocateMemory (priv->device, &mem_alloc_info, NULL, &priv->depth_attachment.memory)))
      return false;
    if (!gtk_vulkan_area_vk_check_fatal (vkBindImageMemory (priv->device, priv->depth_attachment.image, priv->depth_attachment.memory, 0)))
      return false;
  }

  VkImageViewCreateInfo color_image_view;
  color_image_view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  color_image_view.pNext = NULL;
  color_image_view.flags = 0;
  color_image_view.image = priv->color_attachment.image;
  color_image_view.viewType = VK_IMAGE_VIEW_TYPE_2D;
  color_image_view.format = priv->color_format;
  color_image_view.components.r = 0;
  color_image_view.components.g = 0;
  color_image_view.components.b = 0;
  color_image_view.components.a = 0;
  color_image_view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  color_image_view.subresourceRange.baseMipLevel = 0;
  color_image_view.subresourceRange.levelCount = 1;
  color_image_view.subresourceRange.baseArrayLayer = 0;
  color_image_view.subresourceRange.layerCount = 1;

  if (!gtk_vulkan_area_vk_check_fatal (vkCreateImageView (priv->device, &color_image_view, NULL, &priv->color_attachment.view)))
    return false;

  VkImageViewCreateInfo depth_stencil_view;
  depth_stencil_view.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  depth_stencil_view.pNext = NULL;
  depth_stencil_view.flags = 0;
  depth_stencil_view.image = priv->depth_attachment.image;
  depth_stencil_view.viewType = VK_IMAGE_VIEW_TYPE_2D;
  depth_stencil_view.format = priv->depth_format;
  depth_stencil_view.components.r = 0;
  depth_stencil_view.components.g = 0;
  depth_stencil_view.components.b = 0;
  depth_stencil_view.components.a = 0;
  depth_stencil_view.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
  depth_stencil_view.subresourceRange.baseMipLevel = 0;
  depth_stencil_view.subresourceRange.levelCount = 1;
  depth_stencil_view.subresourceRange.baseArrayLayer = 0;
  depth_stencil_view.subresourceRange.layerCount = 1;
  if (!gtk_vulkan_area_vk_check_fatal (vkCreateImageView (priv->device, &depth_stencil_view, NULL, &priv->depth_attachment.view)))
    return false;

  return true;
}

bool
gtk_vulkan_area_create_framebuffer (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  VkImageView attachments[2];
  attachments[0] = priv->color_attachment.view;
  attachments[1] = priv->depth_attachment.view;

  VkFramebufferCreateInfo framebuffer_create_info;
  framebuffer_create_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  framebuffer_create_info.pNext = NULL;
  framebuffer_create_info.flags = 0;
  framebuffer_create_info.renderPass = priv->render_pass;
  framebuffer_create_info.attachmentCount = 2;
  framebuffer_create_info.pAttachments = attachments;
  framebuffer_create_info.width = priv->width;
  framebuffer_create_info.height = priv->height;
  framebuffer_create_info.layers = 1;
  if (!gtk_vulkan_area_vk_check_fatal (vkCreateFramebuffer (priv->device, &framebuffer_create_info, NULL, &priv->framebuffer)))
    return false;

  return true;
}

void
gtk_vulkan_area_destroy_framebuffer (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  if (priv->framebuffer)
    vkDestroyFramebuffer (priv->device, priv->framebuffer, NULL);
}

static void
gtk_vulkan_area_set_property (GObject *gobject,
                              guint prop_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  GtkVulkanArea *self = GTK_VULKAN_AREA (gobject);

  switch (prop_id)
    {
    case PROP_APPLICATION_NAME:
      gtk_vulkan_area_set_application_name (self, g_value_get_string (value));
      break;

    case PROP_ENGINE_NAME:
      gtk_vulkan_area_set_engine_name (self, g_value_get_string (value));
      break;

    case PROP_RENDER_PERMANENTLY:
      gtk_vulkan_area_set_render_permanently (self, g_value_get_boolean (value));
      break;

    case PROP_EXTENSIONS:
      gtk_vulkan_area_set_extensions (self, g_value_get_string (value));
      break;

    case PROP_VALIDATION_LAYERS:
      gtk_vulkan_area_set_validation_layers (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
    }
}

static void
gtk_vulkan_area_get_property (GObject *gobject,
                              guint prop_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (GTK_VULKAN_AREA (gobject));

  switch (prop_id)
    {
    case PROP_APPLICATION_NAME:
      g_value_set_string (value, priv->application_name);
      break;

    case PROP_ENGINE_NAME:
      g_value_set_string (value, priv->engine_name);
      break;

    case PROP_RENDER_PERMANENTLY:
      g_value_set_boolean (value, priv->tick_id != 0);
      break;

    case PROP_EXTENSIONS:
      g_value_set_string (value, priv->extensions);
      break;

    case PROP_VALIDATION_LAYERS:
      g_value_set_string (value, priv->validation_layers);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (gobject, prop_id, pspec);
    }
}

static void
gtk_vulkan_area_realize (GtkWidget *widget)
{
  GtkVulkanArea *area = GTK_VULKAN_AREA (widget);
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  GTK_WIDGET_CLASS (gtk_vulkan_area_parent_class)->realize (widget);

  priv->initialized = FALSE;
  if (!gtk_vulkan_area_create_instance (area))
    return;
  if (!gtk_vulkan_area_create_physical_device (area))
    return;
  if (!gtk_vulkan_area_set_depth_format (area))
    return;
  if (!gtk_vulkan_area_create_device_queue_and_command_pool (area))
    return;
  if (!gtk_vulkan_area_create_render_pass (area))
    return;

  vkQueueWaitIdle (priv->queue);
  vkDeviceWaitIdle (priv->device);

  priv->initialized = TRUE;
  priv->needs_resize = TRUE;
}

static void
gtk_vulkan_area_notify (GObject *object,
                        GParamSpec *pspec)
{
  if (strcmp (pspec->name, "scale-factor") == 0)
    {
      GtkVulkanArea *area = GTK_VULKAN_AREA (object);
      GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

      priv->needs_resize = TRUE;
    }

  if (G_OBJECT_CLASS (gtk_vulkan_area_parent_class)->notify)
    G_OBJECT_CLASS (gtk_vulkan_area_parent_class)->notify (object, pspec);
}

static void
gtk_vulkan_area_resize (GtkVulkanArea *area, int width, int height)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  priv->width = width;
  priv->height = height;

  if (priv->initialized)
    {
      priv->needs_resize = TRUE;

      gtk_vulkan_area_destroy_framebuffer_attachments (area);
      if (!gtk_vulkan_area_create_framebuffer_attachments (area))
        return;
      gtk_vulkan_area_destroy_framebuffer (area);
      if (!gtk_vulkan_area_create_framebuffer (area))
        return;

      vkDestroyImage (priv->device, priv->dst_image, NULL);
      priv->dst_image = NULL;

      vkQueueWaitIdle (priv->queue);
      vkDeviceWaitIdle (priv->device);

      priv->needs_resize = FALSE;
    }
}

static void
gtk_vulkan_area_unrealize (GtkWidget *widget)
{
  GtkVulkanArea *area = GTK_VULKAN_AREA (widget);
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  gtk_vulkan_area_destroy_framebuffer_attachments (area);
  gtk_vulkan_area_destroy_framebuffer (area);

  if (priv->application_name)
    free (priv->application_name);
  if (priv->engine_name)
    free (priv->engine_name);
  if (priv->validation_layers)
    free (priv->validation_layers);
  if (priv->extensions)
    free (priv->extensions);

  GTK_WIDGET_CLASS (gtk_vulkan_area_parent_class)->unrealize (widget);
}

static void
gtk_vulkan_area_size_allocate (GtkWidget *widget,
                               int width,
                               int height,
                               int baseline)
{
  GtkVulkanArea *area = GTK_VULKAN_AREA (widget);
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  GTK_WIDGET_CLASS (gtk_vulkan_area_parent_class)->size_allocate (widget, width, height, baseline);

  if (gtk_widget_get_realized (widget))
    priv->needs_resize = TRUE;
}

static void
gtk_vulkan_area_submit_work (GtkVulkanArea *area, VkCommandBuffer cmdBuffer, VkQueue queue)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  VkSubmitInfo submit_info;
  submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.pNext = NULL;
  submit_info.waitSemaphoreCount = 0;
  submit_info.pWaitSemaphores = NULL;
  submit_info.pWaitDstStageMask = NULL;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers = &cmdBuffer;
  submit_info.signalSemaphoreCount = 0;
  submit_info.pSignalSemaphores = NULL;

  VkFenceCreateInfo fence_create_info;
  fence_create_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
  fence_create_info.pNext = NULL;
  fence_create_info.flags = 0;

  VkFence fence;
  if (!gtk_vulkan_area_vk_check_fatal (vkCreateFence (priv->device, &fence_create_info, NULL, &fence)))
    return;
  if (!gtk_vulkan_area_vk_check_fatal (vkQueueSubmit (queue, 1, &submit_info, fence)))
    return;
  if (!gtk_vulkan_area_vk_check_fatal (vkWaitForFences (priv->device, 1, &fence, VK_TRUE, UINT64_MAX)))
    return;
  vkDestroyFence (priv->device, fence, NULL);
};

static void
gtk_vulkan_area_snapshot (GtkWidget *widget,
                          GtkSnapshot *snapshot)
{
  GtkVulkanArea *area = GTK_VULKAN_AREA (widget);
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  int scale = gtk_widget_get_scale_factor (widget);
  int w = gtk_widget_get_width (widget) * scale;
  int h = gtk_widget_get_height (widget) * scale;

  if (w == 0 || h == 0 || !priv->initialized)
    return;

  if (priv->needs_render)
    {
      if (priv->needs_resize)
        {
          g_signal_emit (area, area_signals[RESIZE], 0, w, h);
        }

      priv->needs_render = FALSE;

      {
        // Command buffer for copy commands (reused)
        VkCommandBufferAllocateInfo command_buffer_allocate_info;
        command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        command_buffer_allocate_info.pNext = NULL;
        command_buffer_allocate_info.commandPool = priv->command_pool;
        command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        command_buffer_allocate_info.commandBufferCount = 1;

        VkCommandBuffer copy_cmd;
        if (!gtk_vulkan_area_vk_check_fatal (vkAllocateCommandBuffers (priv->device, &command_buffer_allocate_info, &copy_cmd)))
          return;

        VkCommandBufferBeginInfo cmd_buffer_begin_info;
        cmd_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        cmd_buffer_begin_info.pNext = NULL;
        cmd_buffer_begin_info.flags = 0;
        cmd_buffer_begin_info.pInheritanceInfo = NULL;

        if (!gtk_vulkan_area_vk_check_fatal (vkBeginCommandBuffer (copy_cmd, &cmd_buffer_begin_info)))
          return;

        VkClearColorValue clear_color = { { 1, 1, 1, 1 } };

        VkClearValue clear_values[3] = {};
        clear_values[0].color = clear_values[2].color = clear_color;
        clear_values[1].depthStencil.depth = 1;
        clear_values[1].depthStencil.stencil = 0;

        VkRenderPassBeginInfo rp_begin_info = {};
        rp_begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        rp_begin_info.pNext = NULL;
        rp_begin_info.renderPass = priv->render_pass;
        rp_begin_info.framebuffer = priv->framebuffer;
        rp_begin_info.renderArea.extent.width = priv->width;
        rp_begin_info.renderArea.extent.height = priv->height;
        rp_begin_info.clearValueCount = priv->swap_chain_size;
        rp_begin_info.pClearValues = clear_values;

        // starting render pass with secondary command buffer support
        vkCmdBeginRenderPass (copy_cmd, &rp_begin_info, VK_SUBPASS_CONTENTS_INLINE);

        priv->current_command_buffer = copy_cmd;
        g_signal_emit (area, area_signals[RENDER], 0, copy_cmd, priv->current_frame);

        vkCmdEndRenderPass (copy_cmd);

        if (!gtk_vulkan_area_vk_check_fatal (vkEndCommandBuffer (copy_cmd)))
          return;

        gtk_vulkan_area_submit_work (area, copy_cmd, priv->queue);
        vkFreeCommandBuffers (priv->device, priv->command_pool, 1, &copy_cmd);
      }

      if (priv->dst_image == NULL)
        {
          // Create the linear tiled destination image to copy to and to read the memory from
          VkImageCreateInfo image_create_info;
          image_create_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
          image_create_info.pNext = NULL;
          image_create_info.flags = 0;
          image_create_info.imageType = VK_IMAGE_TYPE_2D;
          image_create_info.format = priv->color_format;
          image_create_info.extent.width = priv->width;
          image_create_info.extent.height = priv->height;
          image_create_info.extent.depth = 1;
          image_create_info.mipLevels = 1;
          image_create_info.arrayLayers = 1;
          image_create_info.samples = VK_SAMPLE_COUNT_1_BIT;
          image_create_info.tiling = VK_IMAGE_TILING_LINEAR;
          image_create_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
          image_create_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
          image_create_info.queueFamilyIndexCount = 0;
          image_create_info.pQueueFamilyIndices = NULL;
          image_create_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

          // Create the image
          if (!gtk_vulkan_area_vk_check_fatal (vkCreateImage (priv->device, &image_create_info, NULL, &priv->dst_image)))
            return;
          // Create memory to back up the image
          VkMemoryRequirements mem_requirements;
          vkGetImageMemoryRequirements (priv->device, priv->dst_image, &mem_requirements);

          VkMemoryAllocateInfo mem_alloc_info;
          mem_alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
          mem_alloc_info.pNext = NULL;
          mem_alloc_info.allocationSize = 0;
          mem_alloc_info.memoryTypeIndex = gtk_vulkan_area_get_memory_type_index (mem_requirements.memoryTypeBits,
                                                                                  VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                                                                                  priv->physical_device);
          mem_alloc_info.allocationSize = mem_requirements.size;

          if (priv->dst_image_memory != NULL)
            {
              vkUnmapMemory (priv->device, priv->dst_image_memory);
              vkFreeMemory (priv->device, priv->dst_image_memory, NULL);
              priv->dst_image_memory = NULL;
            }

          // Memory must be host visible to copy from
          if (!gtk_vulkan_area_vk_check_fatal (vkAllocateMemory (priv->device, &mem_alloc_info, NULL, &priv->dst_image_memory)))
            return;
          if (!gtk_vulkan_area_vk_check_fatal (vkBindImageMemory (priv->device, priv->dst_image, priv->dst_image_memory, 0)))
            return;

          vkMapMemory (priv->device, priv->dst_image_memory, 0, VK_WHOLE_SIZE, 0, (void **) &priv->pixels);
        }

      // Do the actual blit from the offscreen image to our host visible destination image
      VkCommandBufferAllocateInfo command_buffer_allocate_info;
      command_buffer_allocate_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
      command_buffer_allocate_info.pNext = NULL;
      command_buffer_allocate_info.commandPool = priv->command_pool;
      command_buffer_allocate_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
      command_buffer_allocate_info.commandBufferCount = 1;

      VkCommandBuffer copy_cmd;
      if (!gtk_vulkan_area_vk_check_fatal (vkAllocateCommandBuffers (priv->device, &command_buffer_allocate_info, &copy_cmd)))
        return;

      VkCommandBufferBeginInfo cmd_buffer_begin_info;
      cmd_buffer_begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
      cmd_buffer_begin_info.pNext = NULL;
      cmd_buffer_begin_info.flags = 0;
      cmd_buffer_begin_info.pInheritanceInfo = NULL;

      if (!gtk_vulkan_area_vk_check_fatal (vkBeginCommandBuffer (copy_cmd, &cmd_buffer_begin_info)))
        return;

      {
        // Transition destination image to transfer destination layout
        VkImageSubresourceRange range;
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.layerCount = 1;

        gtk_vulkan_area_insert_image_memory_barrier (copy_cmd,
                                                     priv->dst_image,
                                                     0,
                                                     VK_ACCESS_TRANSFER_WRITE_BIT,
                                                     VK_IMAGE_LAYOUT_UNDEFINED,
                                                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                                     VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                     VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                     range);
      }

      VkImageCopy image_copy_region;
      image_copy_region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      image_copy_region.srcSubresource.mipLevel = 0;
      image_copy_region.srcSubresource.baseArrayLayer = 0;
      image_copy_region.srcSubresource.layerCount = 1;
      image_copy_region.srcOffset.x = 0;
      image_copy_region.srcOffset.y = 0;
      image_copy_region.srcOffset.z = 0;
      image_copy_region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      image_copy_region.dstSubresource.mipLevel = 0;
      image_copy_region.dstSubresource.baseArrayLayer = 0;
      image_copy_region.dstSubresource.layerCount = 1;
      image_copy_region.dstOffset.x = 0;
      image_copy_region.dstOffset.y = 0;
      image_copy_region.dstOffset.z = 0;
      image_copy_region.extent.width = priv->width;
      image_copy_region.extent.height = priv->height;
      image_copy_region.extent.depth = 1;

      vkCmdCopyImage (copy_cmd,
                      priv->color_attachment.image,
                      VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                      priv->dst_image,
                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                      1,
                      &image_copy_region);
      {
        VkImageSubresourceRange range;
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.layerCount = 1;

        gtk_vulkan_area_insert_image_memory_barrier (copy_cmd,
                                                     priv->dst_image,
                                                     VK_ACCESS_TRANSFER_WRITE_BIT,
                                                     VK_ACCESS_MEMORY_READ_BIT,
                                                     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                                     VK_IMAGE_LAYOUT_GENERAL,
                                                     VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                     VK_PIPELINE_STAGE_TRANSFER_BIT,
                                                     range);
      }

      if (!gtk_vulkan_area_vk_check_fatal (vkEndCommandBuffer (copy_cmd)))
        return;

      gtk_vulkan_area_submit_work (area, copy_cmd, priv->queue);
      vkFreeCommandBuffers (priv->device, priv->command_pool, 1, &copy_cmd);

      VkImageSubresource sub_resource;
      sub_resource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
      sub_resource.mipLevel = 0;
      sub_resource.arrayLayer = 0;

      VkSubresourceLayout sub_resource_layout;
      vkGetImageSubresourceLayout (priv->device, priv->dst_image, &sub_resource, &sub_resource_layout);

      GBytes *gbytes = g_bytes_new_take (priv->pixels + sub_resource_layout.offset, sub_resource_layout.size);

      GdkTexture *texture = gdk_memory_texture_new (
          priv->width, priv->height, GDK_MEMORY_R8G8B8A8_PREMULTIPLIED, gbytes, sub_resource_layout.rowPitch);

      gtk_snapshot_save (snapshot);
      gtk_snapshot_append_texture (snapshot, texture, &GRAPHENE_RECT_INIT (0, 0, priv->width, priv->height));
      gtk_snapshot_restore (snapshot);

      g_object_unref (texture);
    }

  priv->current_frame++;
  if (priv->current_frame >= priv->swap_chain_size)
    priv->current_frame = 0;
}

static void
gtk_vulkan_area_class_init (GtkVulkanAreaClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  klass->resize = gtk_vulkan_area_resize;

  widget_class->realize = gtk_vulkan_area_realize;
  widget_class->unrealize = gtk_vulkan_area_unrealize;
  widget_class->size_allocate = gtk_vulkan_area_size_allocate;
  widget_class->snapshot = gtk_vulkan_area_snapshot;

  obj_props[PROP_APPLICATION_NAME] =
      g_param_spec_string ("application-name",
                           NULL,
                           NULL,
                           "gtk_vulkan_area",
                           GTK_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS |
                               G_PARAM_EXPLICIT_NOTIFY);

  obj_props[PROP_ENGINE_NAME] =
      g_param_spec_string ("engine-name",
                           NULL,
                           NULL,
                           "gtk_vulkan_area",
                           GTK_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS |
                               G_PARAM_EXPLICIT_NOTIFY);

  obj_props[PROP_RENDER_PERMANENTLY] =
      g_param_spec_boolean ("render-permanently",
                            NULL,
                            NULL,
                            TRUE,
                            GTK_PARAM_READWRITE |
                                G_PARAM_STATIC_STRINGS |
                                G_PARAM_EXPLICIT_NOTIFY);

  obj_props[PROP_EXTENSIONS] =
      g_param_spec_string ("extensions",
                           NULL,
                           NULL,
                           "",
                           GTK_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS |
                               G_PARAM_EXPLICIT_NOTIFY);

  obj_props[PROP_VALIDATION_LAYERS] =
      g_param_spec_string ("validation-layers",
                           NULL,
                           NULL,
                           "",
                           GTK_PARAM_READWRITE |
                               G_PARAM_STATIC_STRINGS |
                               G_PARAM_EXPLICIT_NOTIFY);

  area_signals[RENDER] =
      g_signal_new (I_ ("render"),
                    G_OBJECT_CLASS_TYPE (gobject_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (GtkVulkanAreaClass, render),
                    NULL, NULL,
                    _gtk_marshal_VOID__POINTER_UINT,
                    G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_UINT);
  g_signal_set_va_marshaller (area_signals[RENDER],
                              G_TYPE_FROM_CLASS (klass),
                              _gtk_marshal_VOID__POINTER_UINTv);

  area_signals[RESIZE] =
      g_signal_new (I_ ("resize"),
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (GtkVulkanAreaClass, resize),
                    NULL, NULL,
                    _gtk_marshal_VOID__INT_INT,
                    G_TYPE_NONE, 2, G_TYPE_INT, G_TYPE_INT);
  g_signal_set_va_marshaller (area_signals[RESIZE],
                              G_TYPE_FROM_CLASS (klass),
                              _gtk_marshal_VOID__INT_INTv);

  gobject_class->set_property = gtk_vulkan_area_set_property;
  gobject_class->get_property = gtk_vulkan_area_get_property;
  gobject_class->notify = gtk_vulkan_area_notify;

  g_object_class_install_properties (gobject_class, LAST_PROP, obj_props);
}

static gboolean
gtk_vulkan_tick_callback (GtkWidget *widget,
                          GdkFrameClock *frame_clock,
                          gpointer user_data)
{
  (void) frame_clock;
  (void) user_data;

  GtkVulkanArea *area = GTK_VULKAN_AREA (widget);
  gtk_vulkan_area_queue_render (area);
  return true;
}

static void
gtk_vulkan_area_init (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  priv->width = 0;
  priv->height = 0;

  priv->validation_layers = NULL;
  priv->extensions = NULL;

  priv->color_format = VK_FORMAT_R8G8B8A8_SRGB;
  priv->swap_chain_size = 2;

  priv->current_frame = 0;
  priv->dst_image_memory = NULL;
  priv->pixels = NULL;
  priv->framebuffer = NULL;

  priv->needs_resize = TRUE;
  priv->needs_render = TRUE;
  priv->tick_id = gtk_widget_add_tick_callback (GTK_WIDGET (area), gtk_vulkan_tick_callback, NULL, NULL);
  priv->initialized = FALSE;
}

/**
 * gtk_vulkan_area_new:
 *
 * Creates a new `GtkVulkanArea` widget.
 *
 * Returns: a new `GtkVulkanArea`
 */
GtkWidget *
gtk_vulkan_area_new (void)
{
  GtkVulkanArea *area = g_object_new (GTK_TYPE_VULKAN_AREA, NULL);

  return GTK_WIDGET (area);
}

void
gtk_vulkan_area_set_application_name (GtkVulkanArea *area, const char *application_name)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  if (priv->initialized)
    {
      g_warning ("Cannot set application_name: already initialized");
      return;
    }

  if (application_name && (!priv->application_name || strcmp (priv->application_name, application_name) == 0))
    {
      if (priv->application_name)
        free (priv->application_name);
      priv->application_name = strdup (application_name);

      g_object_notify (G_OBJECT (area), "application_name");
    }
}

const char *
gtk_vulkan_area_get_application_name (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->application_name;
}

void
gtk_vulkan_area_set_engine_name (GtkVulkanArea *area, const char *engine_name)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  if (priv->initialized)
    {
      g_warning ("Cannot set engine_name: already initialized");
      return;
    }

  if (engine_name && (!priv->engine_name || strcmp (priv->engine_name, engine_name) == 0))
    {
      if (priv->engine_name)
        free (priv->engine_name);
      priv->engine_name = strdup (engine_name);

      g_object_notify (G_OBJECT (area), "engine_name");
    }
}

const char *
gtk_vulkan_area_get_engine_name (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->engine_name;
}

void
gtk_vulkan_area_set_validation_layers (GtkVulkanArea *area, const char *validation_layers)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  if (priv->initialized)
    {
      g_warning ("Cannot set validation_layers: already initialized");
      return;
    }

  if (validation_layers && (!priv->validation_layers || strcmp (priv->validation_layers, validation_layers) == 0))
    {
      if (priv->validation_layers)
        free (priv->validation_layers);
      priv->validation_layers = strdup (validation_layers);

      g_object_notify (G_OBJECT (area), "validation_layers");
    }
}

const char *
gtk_vulkan_area_get_validation_layers (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->validation_layers;
}

void
gtk_vulkan_area_set_extensions (GtkVulkanArea *area, const char *extensions)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  if (priv->initialized)
    {
      g_warning ("Cannot set extensions: already initialized");
      return;
    }

  if (extensions && (!priv->extensions || strcmp (priv->extensions, extensions) == 0))
    {
      if (priv->extensions)
        free (priv->extensions);
      priv->extensions = strdup (extensions);

      g_object_notify (G_OBJECT (area), "extensions");
    }
}

const char *
gtk_vulkan_area_get_extensions (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->extensions;
}

void
gtk_vulkan_area_set_render_permanently (GtkVulkanArea *area,
                                        gboolean render_permanently)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  render_permanently = !!render_permanently;

  if ((priv->tick_id != 0) != render_permanently)
    {
      if (render_permanently)
        {
          priv->tick_id = gtk_widget_add_tick_callback (GTK_WIDGET (area), gtk_vulkan_tick_callback, NULL, NULL);
        }
      else
        {
          gtk_widget_remove_tick_callback (GTK_WIDGET (area), priv->tick_id);
          priv->tick_id = 0;
        }

      g_object_notify (G_OBJECT (area), "auto-render");

      if (render_permanently)
        gtk_widget_queue_draw (GTK_WIDGET (area));
    }
}

gboolean
gtk_vulkan_area_get_render_permanently (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return (priv->tick_id != 0);
}

void
gtk_vulkan_area_queue_render (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_if_fail (GTK_IS_VULKAN_AREA (area));

  priv->needs_render = TRUE;

  gtk_widget_queue_draw (GTK_WIDGET (area));
}

uint32_t
gtk_vulkan_area_get_swap_chain_size (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->swap_chain_size;
}

int
gtk_vulkan_area_get_width (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->width;
}

int
gtk_vulkan_area_get_height (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->height;
}

VkInstance
gtk_vulkan_area_get_instance (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->instance;
}

VkPhysicalDevice
gtk_vulkan_area_get_physical_device (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->physical_device;
}

VkDevice
gtk_vulkan_area_get_device (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->device;
}

VkQueue
gtk_vulkan_area_get_queue (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->queue;
}

VkCommandPool
gtk_vulkan_area_get_command_pool (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->command_pool;
}

VkRenderPass
gtk_vulkan_area_get_render_pass (GtkVulkanArea *area)
{
  GtkVulkanAreaPrivate *priv = gtk_vulkan_area_get_instance_private (area);

  g_return_val_if_fail (GTK_IS_VULKAN_AREA (area), FALSE);

  return priv->render_pass;
}
